import TimeAgo from 'preact-timeago'
import timeago from 'timeago.js'
import timeagoLocaleDE from 'timeago.js/locales/de'

timeago.register('de', timeagoLocaleDE)

export default class TimeAgoGerman extends TimeAgo {
  componentWillMount () {
    super.componentWillMount()
    this.instance.setLocale('de')
  }
}
