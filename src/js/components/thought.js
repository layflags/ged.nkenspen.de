import { dateToDEString, urlify, escapeHtml, commandify } from '../helpers.js'
import TimeAgoGerman from './time-ago-german'
import preact from 'preact' // eslint-disable-line

export default function Thought ({ timestamp, title, support }) {
  const date = new Date(timestamp)
  const isInvalidDate = isNaN(date.getTime())
  const isoDate = isInvalidDate ? '?' : date.toISOString()
  const humanDate = isInvalidDate ? '?' : dateToDEString(date)
  const titleHtmlEscaped = escapeHtml(title)
  const titleHtml = commandify(title, support) || urlify(titleHtmlEscaped)

  return (
    <li class='Thoughts-item Thought'>
      <span class='Thought-at' title={humanDate}>
        {isInvalidDate ? '?' : <TimeAgoGerman datetime={isoDate} live />}
        <time class='Thought-humanDate'>{humanDate}</time>
      </span>
      <span class='Thought-title u-noBreakOut'
        dangerouslySetInnerHTML={{ __html: titleHtml }} />
    </li>
  )
}

