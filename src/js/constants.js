export const OFFLINE_CACHE_VERSION = process.env.OFFLINE_CACHE_VERSION
export const IS_PROD = process.env.NODE_ENV === 'production'
export const CACHE_VERSION = `v1${IS_PROD ? '' : '_dev'}`
export const MAX_THOUGHTS = 42
export const FIREBASE_CONFIG = IS_PROD
  ? { /* PRODUCTION */
    apiKey: 'AIzaSyBli_gkn6W96fo8CvkA8YCIeEL7hcG00Es',
    // authDomain: "gedankenspende.firebaseapp.com",
    databaseURL: '//gedankenspende.firebaseio.com'
    // storageBucket: "gedankenspende.appspot.com",
    // messagingSenderId: "144943557294"
  }
  : { /* DEVELOPMENT */
    apiKey: 'AIzaSyDqlWSlBboyxjlVWSOs7PwmCAGK_DkoHn0',
    // authDomain: "gedankenspende-dev.firebaseapp.com",
    databaseURL: '//gedankenspende-dev.firebaseio.com'
    // storageBucket: "gedankenspende-dev.appspot.com",
    // messagingSenderId: "427514026544"
  }

