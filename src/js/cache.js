export default function createCache () {
  return {
    getItem,
    setItem
  }

  function setItem (key, value) {
    if (!('localStorage' in window)) return
    window.localStorage.setItem(key, JSON.stringify(value))
  }

  function getItem (key) {
    if (!('localStorage' in window)) return

    try {
      return JSON.parse(window.localStorage.getItem(key))
    } catch (e) {
      return
    }
  }
}
