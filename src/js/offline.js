// 'upup' package doesn't export something, so we have to access it globally
// and check if it is defined on window, because it's just available when
// service workers are supported by the browser.

import { OFFLINE_CACHE_VERSION } from './constants.js'
import 'upup'
const UpUp = window.UpUp || /* monkey mock */ { start: Function.prototype }

UpUp.start({
  'cache-version': OFFLINE_CACHE_VERSION,
  'content-url': '/index.html',
  'assets': [
    '/app.min.js',
    '/index.css',
    '/android-chrome-192x192.png',
    '/android-chrome-512x512.png',
    '/apple-touch-icon.png',
    '/browserconfig.xml',
    '/favicon-16x16.png',
    '/favicon-32x32.png',
    '/favicon.ico',
    '/manifest.json',
    '/mstile-150x150.png',
    '/safari-pinned-tab.svg',
    '/fb-logo.svg'
  ]
})
