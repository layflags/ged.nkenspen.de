import * as slashCommands from './slash-commands.js'

const CMD_REGEX = /^\s*\/(\w+)\s+(.+)$/
const URL_REGEX = /https?:\/{2,}([^\s]+)/gi
const IMAGE_URL_REGEX = /https?:\/{2,}([^\s]+\.(jpg|png|gif))/gi

// misc. helpers

const isSupportCommand = cmdName => cmdName.match(/^support/)

export function escapeHtml (str) {
  const div = document.createElement('div')
  div.appendChild(document.createTextNode(str))
  return div.innerHTML
}

export function dateToDEString (date) {
  const d = date.getDate()
  const m = date.getMonth() + 1
  const y = date.getFullYear()
  const h = ('0' + date.getHours()).substr(-2, 2)
  const min = ('0' + date.getMinutes()).substr(-2, 2)

  return `${d}.${m}.${y}, ${h}:${min} Uhr`
}

export function commandify (text, allowSupportCommands) {
  const cmdMatch = text.match(CMD_REGEX)
  if (!cmdMatch) return

  const [, cmdName, cmdValue] = cmdMatch
  const command = slashCommands[cmdName]

  if (isSupportCommand(cmdName) && !allowSupportCommands) return '*****'

  return command && command(cmdValue)
}

// URL helpers

export const isImageUrl = str => str.match(IMAGE_URL_REGEX)

export const urlify = text => text
  .replace(URL_REGEX, '<a href="$&" rel="noopener" target="_blank">$1</a>')

export const stripProtocol = url => url
  .replace(/^https?:\/\//, '')

export const querify = options => Object.keys(options)
  .map(key => `${escape(key)}=${escape(options[key])}`)
  .join('&')

export const rszioify = (url, options) =>
  `//rsz.io/${stripProtocol(url)}${options ? `?${querify(options)}` : ''}`

