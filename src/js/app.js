import 'core-js/fn/object/assign'

import './offline.js'
import createCache from './cache.js'
import createStore from './store.js'
import createView from './view.js'
import debounce from 'lodash.debounce'
import { CACHE_VERSION } from './constants.js'
import { triggerGAEvent } from './tracking.js'

const cache = createCache()
const storeCacheKey = `storeCache_${CACHE_VERSION}`
const store = createStore(cache.getItem(storeCacheKey))
const container = document.getElementById('app')
const view = createView(container, store.getState())

store.onUpdateState(debounce(state => {
  view.render(state)
  cache.setItem(storeCacheKey, state)
}, 5))

view.onSubmitThought(data => {
  store.createThought(data)
  triggerGAEvent({ category: 'Thought', action: 'submit' })
})

view.onClickFeedbackLink(() => {
  triggerGAEvent({ category: 'FeedbackEmail', action: 'click' })
})

view.onClickOutboundLink(href => {
  triggerGAEvent({ category: 'OutboundLink', action: 'click', label: href })
})

