import { urlify, isImageUrl, rszioify, escapeHtml } from './helpers.js'

//  format: /image <image url>
// example: /image http://some.url/image.png
export function image (rawValue) {
  if (!isImageUrl(rawValue)) return

  const imageUrl = escapeHtml(rawValue)
  const rszioifiedImageUrl =
    rszioify(imageUrl, { width: 672, height: 480, mode: 'max', quality: 85 })

  return `
    <figure>
      <img src="${rszioifiedImageUrl}" class="u-imgResponsive" />
      <figcaption>Quelle: ${urlify(imageUrl)}</figcaption>
    </figure>`.trim()
}

//  format: /quote <text>[ -- <author or source>]
// example: /quote E=mc² -- Albert Einstein
export function quote (rawValue) {
  const [rawQuote, rawCite] = rawValue.split('--')
  const cite = rawCite && rawCite.trim()
    ? `<cite>${escapeHtml(rawCite).trim()}</cite>`
    : ''

  return `<q>${escapeHtml(rawQuote).trim()}</q>${cite}`
}

// SUPPORT COMMANDS ////////////////////////////////////////////////////////////

//  format: /support <text>
// example: /support We had to delete some entries because of bad language
export function support (rawValue) {
  return `
    <div class="SupportBox">
      ${escapeHtml(rawValue)}
      <div class="SupportBox-footer">Euer ged@nkenspen.de Support</em>
    </div>`.trim()
}

