import firebase from 'firebase/app'
import 'firebase/database'
import { FIREBASE_CONFIG, MAX_THOUGHTS } from './constants.js'

export default function createStore (initialStateFromCache) {
  let state = initialStateFromCache ? setCacheFlag(initialStateFromCache) : {}
  let updateStateCallback = Function.prototype

  const { postThought } = connectToFirebase({
    onReceiveThought: receiveThought
  })

  return {
    getState,
    onUpdateState,
    createThought
  }

  function getState () {
    return state
  }

  function onUpdateState (callback) {
    updateStateCallback = callback
  }

  function createThought ({ title }) {
    const newTitle = title.trim()

    // ignore empty title
    if (!newTitle) return

    if (/^[.,/-]+$/.test(newTitle)) {
      const timestamp = Date.now()
      receiveThought({ title: newTitle, key: timestamp, timestamp })
      return
    }

    postThought(newTitle)
  }

  /* private */

  function setState (newState) {
    const oldState = state

    if (oldState === newState) return

    state = newState
    updateStateCallback(newState, oldState)
  }

  function receiveThought (thought) {
    const oldEntry = state[thought.key]
    const isFromCache = oldEntry && oldEntry.fromCache

    // copy current state (and eventually remove cache entries)
    let newState = isFromCache ? removeCacheEntries(state) : { ...state }

    // add entry
    newState[thought.key] = thought

    // remove oldest entry
    if (Object.keys(newState).length > MAX_THOUGHTS) {
      newState = removeOldestEntry(newState)
    }

    setState(newState)
  }
}

function connectToFirebase ({ onReceiveThought = Function.prototype }) {
  firebase.initializeApp(FIREBASE_CONFIG)

  const db = firebase.database()
  const thoughtsRef = db.ref('thoughts')

  thoughtsRef
    .orderByChild('timestamp')
    .limitToLast(MAX_THOUGHTS)
    .on('child_added', childData => {
      const data = childData.val()
      onReceiveThought({ ...data, key: childData.key })
    })

  return {
    postThought (title) {
      thoughtsRef.push().set({
        title,
        timestamp: firebase.database.ServerValue.TIMESTAMP
      })
    }
  }
}

function setCacheFlag (state) {
  const cacheFlag = { fromCache: true }
  return Object.keys(state).reduce((result, key) => {
    result[key] = Object.assign({}, state[key], cacheFlag)
    return result
  }, {})
}

function removeOldestEntry (state) {
  const keys = Object.keys(state)
  const oldestTimestamp = keys.map(key => state[key].timestamp).sort()[0]
  const oldestKey = keys.find(key => state[key].timestamp === oldestTimestamp)
  const newState = { ...state }
  delete newState[oldestKey]
  return newState
}

function removeCacheEntries (state) {
  return Object.keys(state).reduce((result, key) => {
    if (result[key].fromCache) delete result[key]
    return result
  }, { ...state })
}

function timestampDesc (a, b) {
  if (a.timestamp > b.timestamp) return -1
  if (a.timestamp < b.timestamp) return 1
  return 0
}

export function stateToListSortedByTimestampDesc (state) {
  const keys = Object.keys(state)
  return keys.map(key => state[key]).sort(timestampDesc)
}
