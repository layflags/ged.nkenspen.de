export function triggerGAEvent ({ category, action, label }) {
  if (!window.ga) return

  window.ga('send', 'event', {
    eventCategory: category,
    eventAction: action,
    eventLabel: label
  })
}

