import { stateToListSortedByTimestampDesc } from './store.js'
import Thought from './components/thought.js'
import preact from 'preact'

export default function createView (container, initialState) {
  const dom = {
    thoughtsList: container.querySelector('#thoughtsList'),
    thoughtsCount: container.querySelector('#thoughtsCount'),
    contentForm: container.querySelector('#contentForm'),
    titleInput: container.querySelector('#titleInput'),
    titleCounter: container.querySelector('#titleCounter'),
    feedbackLink: container.querySelector('#feedbackLink')
  }

  render(initialState)
  provideInputCounter()

  return {
    render,
    onSubmitThought,
    onClickFeedbackLink,
    onClickOutboundLink
  }

  function render (state) {
    const thoughts = stateToListSortedByTimestampDesc(state)

    // do not render if state is empty (keep loading message)
    if (!thoughts.length) return

    // update Counter
    dom.thoughtsCount.innerText = thoughts.length

    // render List
    preact.render((
      <ul>{thoughts.map(data => <Thought {...data} />)}</ul>
    ), dom.thoughtsList, dom.thoughtsList.lastChild)
  }

  function onSubmitThought (callback) {
    dom.contentForm.addEventListener('submit', event => {
      callback({ title: dom.titleInput.value })
      dom.titleInput.value = ''
      setTitleCounter(0)
      event.preventDefault()
    })
  }

  function onClickFeedbackLink (callback) {
    dom.feedbackLink.addEventListener('click', () => {
      callback()
    })
  }

  function onClickOutboundLink (callback) {
    let clicked = false

    container.addEventListener('click', event => {
      const node = event.target

      if (node.href && node.href.match(/^https?:\/\//) && !clicked) {
        event.preventDefault()
        callback(node.href)
        setTimeout(() => {
          clicked = true
          node.click()
          clicked = false
        }, 200)
      }
    })
  }

  function provideInputCounter () {
    const maxLength = dom.titleInput.getAttribute('maxlength')

    dom.titleInput.addEventListener('input', () => {
      const currentLength = dom.titleInput.value.length
      const progress = 100 / maxLength * currentLength

      setTitleCounter(progress)
    })
  }

  function setTitleCounter (progress) {
    dom.titleCounter.style.width = `${progress}%`
  }
}
