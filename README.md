# ged@nkenspen.de - Spende einen Gedanken!

Please visit [geda.nkenspen.de](https://geda.nkenspen.de/)!

![](src/static/android-chrome-192x192.png)

- powered by [Firebase](https://firebase.google.com/)
- copyright © 2016 [Belza Digital](http://belza.digital) GmbH
