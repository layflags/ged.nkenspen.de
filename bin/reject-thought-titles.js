#!/usr/bin/env node

const usage = 'Usage: reject-thought-titles.js <filename> <title-regexp>'
const filename = process.argv[2]
const titleRegexpStr = process.argv[3]

if (!filename) {
  console.error('Param filename missing!')
  console.info(usage)
  process.exit(1)
}

if (!titleRegexpStr) {
  console.error('Param title-regexp missing!')
  console.info(usage)
  process.exit(1)
}

const titleRegexp = new RegExp(titleRegexpStr)
const data = require(`${process.cwd()}/${filename}`)
const rejectTitles = (titleRegexp, thoughts) =>
  Object.keys(thoughts)
  .map(k => ({ key: k, data: thoughts[k] }))
  .filter(o => !titleRegexp.test(o.data.title))
  .reduce((r, d) => { r[d.key] = d.data; return r }, {})

const newData = rejectTitles(titleRegexp, data)
const output = JSON.stringify(newData)

process.stdout.write(output)

