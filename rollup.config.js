import buble from 'rollup-plugin-buble'
import uglify from 'rollup-plugin-uglify'
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import replace from 'rollup-plugin-replace'

const NODE_ENV = process.env.NODE_ENV || 'development'
const IS_PROD = NODE_ENV === 'production'

console.log('NODE_ENV', NODE_ENV)

export default {
  entry: 'src/js/app.js',
  format: 'iife',
  sourceMap: !IS_PROD,
  plugins: [
    buble({
      objectAssign: 'Object.assign',
      jsx: 'preact.h'
    }),
    replace({
      values: {
        'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
        'process.env.OFFLINE_CACHE_VERSION': JSON.stringify(Date.now())
      }
    }),
    nodeResolve(),
    commonjs(),
    uglify()
  ],
  moduleContext: {
    'node_modules/upup/dist/upup.min.js': 'window'
  },
  useStrict: false, // to ensure firebase can be imported
  dest: 'build/app.min.js'
}
